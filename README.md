# MAIV Plan

Manufacturing, Assembly, Integration and Verification Plan for the AcubeSAT Mission

## Disclaimer

The AcubeSAT project is carried out with the support of the Education Office of
the [European Space Agency](https://esa.int), under the educational
[Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/)
programme.

This repository has been authored by university students participating in the
AcubeSAT project. The views expressed herein by the authors can in no way be
taken to reflect the official opinion, or endorsement, of the European Space
Agency.

## Useful Links
**Budgets**
- [Power Budget](https://docs.google.com/spreadsheets/d/1qEUhpwKS0i_BTmZUl0Qdw5hg4MGTjfWLeKBO6Ngjmls/edit#gid=0)
- [Mass Budget](https://docs.google.com/spreadsheets/d/18Dj1KoFepvAqTrsyw-4qNs1tJcV3xMjf/edit?usp=drive_link&ouid=100261791043536830662&rtpof=true&sd=true)
- [Cost Budget](https://docs.google.com/spreadsheets/d/1b_V7cXYVA-rElzelgIdchJCicu2dHMZ7liASIReQGTs/edit#gid=0)
	- [Payload Procurement - old](https://docs.google.com/spreadsheets/d/1VOMHcc04M8dne_mUooplvE7rvpK6QCm087xDK4GZsEc/edit#gid=1704546329)
    - [Payload EQM BOM](https://docs.google.com/spreadsheets/d/14GuuzHfLxuyXYpjJFvaGn7Jhla3P2d3BC_UBYS1rokk/edit?gid=1096580690#gid=1096580690)
- [Memory Budget](https://docs.google.com/spreadsheets/d/1LI8sU1QJyFAagIMfvU7yWo-klYzEd0hMhoTDFA_ixkA/edit#gid=0)


**Electrical Configuration**
- [PC104](https://docs.google.com/spreadsheets/d/1uwgcFsb0KrEsZPAQ2DLbnALrsRJr7-VwNUPQhVw5wLE/edit?pli=1#gid=0)
- [System Harness](https://docs.google.com/spreadsheets/d/1yLPYQO_D9TkPLalNhvvK20bf97OZsaw-H0YucY9ZBF8/edit#gid=563481210)
- [EPS Channel Allocation](https://docs.google.com/spreadsheets/d/1YKmJlOS9_JIw3WK4ys-ncp_9o0mgkynzZUNN8Q18Roc/edit?gid=0#gid=0)
- [EPS MPPT Allocation](https://gitlab.com/acubesat/eps/organisational/-/issues/42#note_2260095394)

**Documentation**
- [TS-VCD](https://docs.google.com/spreadsheets/d/1juBvdpzv1_lvrgJYr9hH4NrtWYVKF5lG4J2ryPn65Bw/edit#gid=1830179052)
- [RIDs](https://docs.google.com/spreadsheets/d/1sEyA1YjorULCmGG9rDUnFUJsQGUdYFsiCNMMqegbzdc/edit#gid=0)
- [Payload Logbook](https://docs.google.com/spreadsheets/d/1VHvc068LzDJsRm7im6qnMXXS6WXMQGweoi_HsgSD_pk/edit?gid=0#gid=0)


