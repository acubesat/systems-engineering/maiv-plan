# Subsystems Development and Verification Plan

This repository includes development flow diagrams for all AcubeSAT subsystems and components. The template used to create these diagrams can be found below:

![](./Development Flow Template.svg)

## Disclaimer

The AcubeSAT project is carried out with the support of the Education Office of
the [European Space Agency](https://esa.int), under the educational
[Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/)
programme.

This repository has been authored by university students participating in the
AcubeSAT project. The views expressed herein by the authors can in no way be
taken to reflect the official opinion, or endorsement, of the European Space
Agency.