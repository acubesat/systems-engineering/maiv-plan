# AcubeSAT PFM Assembly & Integration

Given the complexity of the system, the assembly and integration block diagrams are split into three ones, namely:

1. Payload Container
2. Subsystem Stack
3. AcubeSAT PFM

The first two (paylaod container, subsystem stack) are to be integrated with the frame to result in the AcubeSAT PFM.

## Payload Container 

![](./Payload_Container.svg)

## Subsystem Stack

![](./Subsystem_Stack.svg)

## AcubeSAT PFM

![](./AcubeSAT_PFM.svg)

## Disclaimer

The AcubeSAT project is carried out with the support of the Education Office of
the [European Space Agency](https://esa.int), under the educational
[Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/)
programme.

This repository has been authored by university students participating in the
AcubeSAT project. The views expressed herein by the authors can in no way be
taken to reflect the official opinion, or endorsement, of the European Space
Agency.